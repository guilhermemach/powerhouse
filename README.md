#1 - Tecnologies and Tools #

- Java8

- Eclipse IDE

- Maven 3.3.9

#2 - Rest Service - Json format (Fraction/Profile) #

- http://localhost:8080/fraction (get)  > return all franction;

Sample:


```
#!json

[{"id":1,"profile":"B","value":0.05,"month":"JAN","errorMessage":null},
{"id":2,"profile":"B","value":0.05,"month":"FEB","errorMessage":null},
{"id":3,"profile":"B","value":0.05,"month":"MAR","errorMessage":null},
{"id":4,"profile":"B","value":0.05,"month":"APR","errorMessage":null},
{"id":5,"profile":"B","value":0.1,"month":"MAI","errorMessage":null},
{"id":6,"profile":"B","value":0.1,"month":"JUN","errorMessage":null},
{"id":7,"profile":"B","value":0.1,"month":"JUL","errorMessage":null},
{"id":8,"profile":"B","value":0.1,"month":"AUG","errorMessage":null},
{"id":9,"profile":"B","value":0.1,"month":"SEP","errorMessage":null},
{"id":10,"profile":"B","value":0.1,"month":"OCT","errorMessage":null},
{"id":11,"profile":"B","value":0.1,"month":"NOV","errorMessage":null},
{"id":12,"profile":"B","value":0.1,"month":"DEC","errorMessage":null}]
```

- http://localhost:8080/fraction/{id} (get) > return one object or a blank response;

Sample:

```
#!json

{"id":8,"profile":"B","value":0.1,"month":"AUG","errorMessage":null}
```



- http://localhost:8080/fraction (post) > save a list of fractions;

Sample:

```
#!json


[{"profile":"B","month":"APR","value":0.05},
{"profile":"B","month":"JAN","value":0.05},
{"profile":"A","month":"JAN","value":0.05},
{"profile":"A","month":"FEB","value":0.05},
{"profile":"A","month":"DEC","value":0.1},
{"profile":"B","month":"FEB","value":0.05},
{"profile":"A","month":"MAI","value":0.1},
{"profile":"A","month":"APR","value":0.05},
{"profile":"B","month":"MAR","value":0.05},
{"profile":"A","month":"JUN","value":0.1},
{"profile":"A","month":"JUL","value":0.8},
{"profile":"A","month":"SEP","value":0.1},
{"profile":"A","month":"AUG","value":0.1},
{"profile":"B","month":"MAI","value":0.1},
{"profile":"B","month":"NOV","value":0.1},
{"profile":"B","month":"DEC","value":0.1},
{"profile":"A","month":"MAR","value":0.05},
{"profile":"A","month":"NOV","value":0.1},
{"profile":"A","month":"OCT","value":0.1},
{"profile":"B","month":"JUL","value":0.1},
{"profile":"B","month":"JUN","value":0.1},
{"profile":"B","month":"SEP","value":0.1},
{"profile":"B","month":"OCT","value":0.1},
{"profile":"B","month":"AUG","value":0.1}]
```


* If there is any incorrect data the method will return only that inconsist block followed by a message error tag , otherside an empty list [] will return to confirm the sucess.

Error Validation Response:

```
#!json

[{"id":null,"profile":"A","value":0.05,"month":"JAN","errorMessage":"Data is incosistent."},
{"id":null,"profile":"A","value":0.05,"month":"FEB","errorMessage":null},
{"id":null,"profile":"A","value":0.05,"month":"MAR","errorMessage":null},
{"id":null,"profile":"A","value":0.05,"month":"APR","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"MAI","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"JUN","errorMessage":null},
{"id":null,"profile":"A","value":0.8,"month":"JUL","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"AUG","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"SEP","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"OCT","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"NOV","errorMessage":null},
{"id":null,"profile":"A","value":0.1,"month":"DEC","errorMessage":null}] 
```



- http://localhost:8080/fraction (put) > update one object (in case of sucess, it will return the updated object).

Sample: 

```
#!json


{"id":8,"profile":"B","value":0.1,"month":"AUG"}
```


- http://localhost:8080/meterreading/{id} (delete) delete one object

# 3 - Rest Service - Json format(Meter Reading) #


http://localhost:8080/meterreading (get)  > return all reading meter;

```
#!json


[{"id":1,"meterID":"0001","profile":"A","month":"JAN","reading":5,"consumption":5,"errorMessage":null},
{"id":2,"meterID":"0001","profile":"A","month":"FEB","reading":11,"consumption":6,"errorMessage":null},
{"id":3,"meterID":"0001","profile":"A","month":"MAR","reading":15,"consumption":4,"errorMessage":null},
{"id":4,"meterID":"0001","profile":"A","month":"APR","reading":21,"consumption":6,"errorMessage":null},
{"id":5,"meterID":"0001","profile":"A","month":"MAI","reading":30,"consumption":9,"errorMessage":null},
{"id":6,"meterID":"0001","profile":"A","month":"JUN","reading":40,"consumption":10,"errorMessage":null},
{"id":7,"meterID":"0001","profile":"A","month":"JUL","reading":50,"consumption":10,"errorMessage":null},
{"id":8,"meterID":"0001","profile":"A","month":"AUG","reading":60,"consumption":10,"errorMessage":null},
{"id":9,"meterID":"0001","profile":"A","month":"SEP","reading":70,"consumption":10,"errorMessage":null},
{"id":10,"meterID":"0001","profile":"A","month":"OCT","reading":80,"consumption":10,"errorMessage":null},
{"id":11,"meterID":"0001","profile":"A","month":"NOV","reading":90,"consumption":10,"errorMessage":null},
{"id":12,"meterID":"0001","profile":"A","month":"DEC","reading":100,"consumption":10,"errorMessage":null}]

```

- http://localhost:8080/meterreading/{id} (get) > return one object or a blank response;

Sample:


```
#!json

{"id":1,"meterID":"0001","profile":"A","month":"JAN","reading":5,"consumption":5,"errorMessage":null}
```



- http://localhost:8080/meterreading (post) > save a list of meter readings;

Sample:


```
#!json

[{"meterID":"0001","profile":"A","month":"JAN","reading":5},
{"meterID":"0001","profile":"A","month":"FEB","reading":11},
{"meterID":"0001","profile":"A","month":"MAR","reading":15},
{"meterID":"0001","profile":"A","month":"APR","reading":21},
{"meterID":"0002","profile":"B","month":"MAI","reading":25},
{"meterID":"0001","profile":"A","month":"MAI","reading":30},
{"meterID":"0001","profile":"A","month":"JUN","reading":40},
{"meterID":"0001","profile":"A","month":"JUL","reading":50},
{"meterID":"0001","profile":"A","month":"AUG","reading":60},
{"meterID":"0001","profile":"A","month":"SEP","reading":70},
{"meterID":"0001","profile":"A","month":"OCT","reading":80},
{"meterID":"0001","profile":"A","month":"NOV","reading":90},
{"meterID":"0001","profile":"A","month":"DEC","reading":100},
{"meterID":"0002","profile":"B","month":"JAN","reading":4},
{"meterID":"0002","profile":"B","month":"FEB","reading":10},
{"meterID":"0002","profile":"B","month":"MAR","reading":15},
{"meterID":"0002","profile":"B","month":"APR","reading":22},
{"meterID":"0002","profile":"B","month":"JUN","reading":30},
{"meterID":"0002","profile":"B","month":"JUL","reading":40},
{"meterID":"0002","profile":"B","month":"AUG","reading":45},
{"meterID":"0002","profile":"B","month":"SEP","reading":60},
{"meterID":"0002","profile":"B","month":"OCT","reading":76},
{"meterID":"0002","profile":"B","month":"NOV","reading":80},
{"meterID":"0002","profile":"B","month":"DEC","reading":120}]
```


* If there is any incorrect data the method will return only that inconsist block followed by a message error tag , otherside an empty list [] will return to confirm the sucess.

Error Validation Response:

```
#!json


[{"id":null,"meterID":"0001","profile":"A","month":"JAN","reading":5,"consumption":5,"errorMessage":"Meter reading with no fraction."},
{"id":null,"meterID":"0001","profile":"A","month":"FEB","reading":11,"consumption":6,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"MAR","reading":15,"consumption":4,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"APR","reading":21,"consumption":6,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"MAI","reading":30,"consumption":9,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"JUN","reading":40,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"JUL","reading":50,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"AUG","reading":60,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"SEP","reading":70,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"OCT","reading":80,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"NOV","reading":90,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0001","profile":"A","month":"DEC","reading":100,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"JAN","reading":4,"consumption":4,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"FEB","reading":10,"consumption":6,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"MAR","reading":19,"consumption":9,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"APR","reading":16,"consumption":0,"errorMessage":"Meter reading is incorrect."},
{"id":null,"meterID":"0002","profile":"B","month":"MAI","reading":25,"consumption":9,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"JUN","reading":30,"consumption":5,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"JUL","reading":40,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"AUG","reading":45,"consumption":5,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"SEP","reading":60,"consumption":15,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"OCT","reading":76,"consumption":16,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"NOV","reading":80,"consumption":4,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"DEC","reading":120,"consumption":40,"errorMessage":null}

```


```
#!json

[{"id":null,"meterID":"0002","profile":"B","month":"JAN","reading":4,"consumption":4,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"FEB","reading":10,"consumption":6,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"MAR","reading":15,"consumption":5,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"APR","reading":22,"consumption":7,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"MAI","reading":25,"consumption":3,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"JUN","reading":30,"consumption":5,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"JUL","reading":40,"consumption":10,"errorMessage":null},
{"id":null,"meterID":"0002","profile":"B","month":"AUG","reading":45,"consumption":5,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"SEP","reading":60,"consumption":15,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"OCT","reading":76,"consumption":16,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"NOV","reading":80,"consumption":4,"errorMessage":"Consumption not allowed."},
{"id":null,"meterID":"0002","profile":"B","month":"DEC","reading":120,"consumption":40,"errorMessage":"Consumption not allowed."}]
```


**
* Messages:**

> Meter reading with no fraction : it will show only one per block of 12.
> Meter reading is incorrect : it will show for each case.
> Consumption not allowed : it will show for each case.


- http://localhost:8080//meterreading (put) > update one object (in case of sucess, it will return the updated object).

Sample: 


```
#!json

{"id":2,"meterID":"0001","profile":"A","month":"FEB","reading":11,"consumption":6}
```



- http://localhost:8080/meterreading/{id} (delete) delete one object


- http://localhost:8080/consumption/{meterID}/{month}

Sample : http://localhost:8080/consumption/0001/JAN


```
#!json

{"id":1,"meterID":"0001","profile":"A","month":"JAN","reading":5,"consumption":5,"errorMessage":null}
```


* consumption tag is the calculated value.


# 4 - Rest Service (Old File Format) #

- it is necessary to place those csv files in 'C:\files\fraction\' for fractions and 'C:\files\meterreading\' for meter reading.

- just pass the final name placed in those foldes. eg. fraction.csv


http://localhost:8080/meterreading/oldfile/{fileName}


http://localhost:8080/fraction/oldfile/{fileName}


Sucess message


```
#!json

{"code":0,"message":""}

```

Error message

```
#!json

{"code":1,"message":"File Not Found = \\files\\fraction\\fraction.csv"}
```


- If any validation occur, it will inform to check de file log. This file will be a csv file with one more colun ('error message');

- File name '{file_to_be_read}_log.csv'; 

- Those message will be treated as same as json format;

Sample:


```
#!csv

MeterID,Profile,Month,Meter Reading,Error Message
0001,A,JAN,5,Meter reading with no fraction.
0001,A,FEB,11,
0001,A,MAR,15,
0001,A,APR,21,
0001,A,MAI,30,
0001,A,JUN,40,
0001,A,JUL,50,
0001,A,AUG,60,
0001,A,SEP,70,
0001,A,OCT,80,
0001,A,NOV,90,
0001,A,DEC,100,
0002,B,JAN,4,Meter reading with no fraction.
0002,B,FEB,10,
0002,B,MAR,19,
0002,B,APR,16,Meter reading is incorrect.
0002,B,MAI,25,
0002,B,JUN,30,
0002,B,JUL,40,
0002,B,AUG,45,
0002,B,SEP,60,
0002,B,OCT,76,
0002,B,NOV,80,
0002,B,DEC,120,
```