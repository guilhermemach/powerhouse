package com.powerhouse.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.powerhouse.constant.Constants;
import com.powerhouse.entity.Fraction;
import com.powerhouse.exception.BusinessException;
import com.powerhouse.repositories.FractionRepository;
import com.powerhouse.rest.Response;

@Service
public class FractionServiceImpl implements FractionService{

	@Autowired
	private FractionRepository repository;

	@Autowired
	private CsvFileService fileService;
	
	public List<Fraction> getAll() {
		return (List<Fraction>) repository.findAll();
	}

	public Fraction getByID(Integer id) {
		return repository.findOne(id);
	}

	public void removeByID(Integer id) {
		repository.delete(id);
	}

	public Fraction saveOrUpdate(Fraction obj) {
		return repository.save(obj);
	}

	public List<Fraction> saveList(List<Fraction> list) {

		Collections.sort(list, Comparator.comparing(Fraction::getProfile).thenComparing(Fraction::getMonth));

		BigDecimal sum = new BigDecimal(0);
		List<Fraction> tempList = new ArrayList<Fraction>();
		List<Fraction> errorList = new ArrayList<Fraction>();
		List<Fraction> finalList = new ArrayList<Fraction>();

		for (int i = 0; i < list.size(); i++) {

			tempList.add(list.get(i));
			sum = sum.add(new BigDecimal(list.get(i).getValue()));

			if ((i + 1) % 12 == 0) {

				if (sum.doubleValue() == 1.0) {
					finalList.addAll(new ArrayList<>(tempList));
				} else {
					tempList.get(0).setErrorMessage(Constants.INCONSISTENT_DATA);
					errorList.addAll(new ArrayList<>(tempList));
				}

				sum = new BigDecimal(0);
				tempList = new ArrayList<>();
			}
		}
		repository.save(finalList);
		return errorList;
	}

	public List<Fraction> findByProfile(String profile) {
		return repository.findByProfile(profile);
	}

	public Response processOldFile(String fileName) {

		List<Fraction> fractionList = null;
		List<Fraction> finalMeterList = null;

		try {
			fractionList = fileService.processOldFraction(fileName);

			finalMeterList = this.saveList(fractionList);

			if (finalMeterList.isEmpty()) {
				fileService.delete(fileName, true);
			} else {
				fileService.writeFractionLog(finalMeterList, fileName + "_log");
			}
		} catch (BusinessException e) {
			return new Response(e.getCode(), e.getMessage());
		}
		if(!finalMeterList.isEmpty()){
			return new Response(Constants.CODE_1, Constants.CHECK_LOG_FILE_MESSAGE);
		}
		return new Response(Constants.CODE_0, "");
	}

}
