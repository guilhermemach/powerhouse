package com.powerhouse.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.stereotype.Component;

/**
 * Service specific for IO file.
 * 
 * @author Guilherme
 *
 */
@Component
public class FileServiceImpl implements FileService {

	public FileReader getFileReader(String path) throws IOException, FileNotFoundException {
		return new FileReader(path);
	}

	public void remove(String fullFileName) {
		File file = new File(fullFileName);

		if (file.exists()) {
			file.delete();
		}
	}
	
	public FileWriter getFileWriter(String path) throws IOException {
		return new FileWriter(path);
	}

}
