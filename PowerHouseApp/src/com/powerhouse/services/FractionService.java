package com.powerhouse.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.powerhouse.entity.Fraction;
import com.powerhouse.rest.Response;

@Service
public interface FractionService {

	public List<Fraction> getAll();

	public Fraction getByID(Integer id);

	public void removeByID(Integer id);

	public Fraction saveOrUpdate(Fraction obj);

	public List<Fraction> saveList(List<Fraction> list);

	public List<Fraction> findByProfile(String profile);

	public Response processOldFile(String fileName);
}
