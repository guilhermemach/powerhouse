package com.powerhouse.services;


import java.util.List;

import org.springframework.stereotype.Service;

import com.powerhouse.constant.MonthShortType;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.rest.Response;

@Service
public interface MeterReadingService {

	public List<MeterReading> getAll();

	public MeterReading getByID(Integer id);

	public MeterReading getConsumptionByMetterIdAndMonth(String meterId, MonthShortType month);

	public List<MeterReading> getMeterReadingByMeterID(String id);

	public void removeByID(Integer id);

	public MeterReading saveOrUpdate(MeterReading obj);

	public List<MeterReading> saveList(List<MeterReading> list);

	public Response processOldFile(String fileName);

}
