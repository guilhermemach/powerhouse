package com.powerhouse.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.powerhouse.entity.Fraction;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.exception.BusinessException;

@Service
public interface CsvFileService {

	public List<Fraction> processOldFraction(String fileName) throws BusinessException;

	public List<MeterReading> processOldMeterReading(String fileName) throws BusinessException;

	public void writeFractionLog(List<Fraction> fractionList, String fileName) throws BusinessException;

	public void writeMeterReadingLog(List<MeterReading> fractionList, String fileName) throws BusinessException;

	public void delete(String fileName, boolean isFractionFile);

}
