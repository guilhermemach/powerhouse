package com.powerhouse.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.powerhouse.constant.MonthShortType;
import com.powerhouse.constant.Constants;
import com.powerhouse.entity.Fraction;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.exception.BusinessException;
import com.powerhouse.repositories.MeterReadingRepository;
import com.powerhouse.rest.Response;

@Component
public class MeterReadingServiceImpl implements MeterReadingService {

	@Autowired
	private MeterReadingRepository repository;

	@Autowired
	private FractionService fractionService;

	@Autowired
	private CsvFileService csvService;
	
	private Map<String,  List<Fraction>> localFractions = new HashMap<String, List<Fraction>>();

	public List<MeterReading> getAll() {
		return (List<MeterReading>) repository.findAll();
	}

	public MeterReading getByID(Integer id) {
		return repository.findOne(id);
	}

	public MeterReading getConsumptionByMetterIdAndMonth(String meterId, MonthShortType month) {
		return repository.getConsumptionByMetterIdAndMonth(meterId, month);
	}

	public List<MeterReading> getMeterReadingByMeterID(String id) {
		return repository.getMeterReadingByMeterId(id);
	}

	public void removeByID(Integer id) {
		repository.delete(id);
	}

	public MeterReading saveOrUpdate(MeterReading obj) {
		return repository.save(obj);
	}

	/**
	 * Validate meter readings and save then. 
	 * In case of error validation, it will return a list of unsaved data followed by their error message.
	 */
	public List<MeterReading> saveList(List<MeterReading> list) {

		Collections.sort(list, Comparator.comparing(MeterReading::getProfile).thenComparing(MeterReading::getMonth));

		Long reading = 0l;
		int count = 0;
		boolean hasError = false;
		List<Fraction> fractions = null;
		List<MeterReading> finalList = new ArrayList<>();
		List<MeterReading> tempList = new ArrayList<>();
		List<MeterReading> errorList = new ArrayList<>();
		Long meterSum = new Long(0);

		for (int i = 0; i < list.size(); i++) {

			tempList.add(list.get(i));

			if (count != 0) {
				if (reading > list.get(i).getReading()) {
					list.get(i).setConsumption(0l);
					list.get(i).setErrorMessage(Constants.DATA_INTEGRITY_PROBLEM);
					hasError = true;
				} else {
					list.get(i).setConsumption(list.get(i).getReading() - reading);
				}

			} else {
				fractions = localFractions.get(list.get(i).getProfile());
				if(fractions == null || fractions.isEmpty()){
					fractions = fractionService.findByProfile(list.get(i).getProfile());
				}

				if (fractions == null || fractions.isEmpty()) {
					list.get(i).setErrorMessage(Constants.DATA_WITH_NO_FRACTION);
					hasError = true;
				}

				list.get(i).setConsumption(list.get(i).getReading());
			}

			reading = list.get(i).getReading();
			count++;
			meterSum += list.get(i).getConsumption();

			if (count == 12) {
				if (hasError) {
					errorList.addAll(new ArrayList<>(tempList));
				
				/* Consumption not allowed validation. */
				} else if (!verifyConsumptionValidation(meterSum, tempList, fractions)) {
					errorList.addAll(new ArrayList<>(tempList));
					
				} else {
					finalList.addAll(new ArrayList<>(tempList));					
				}

				reading = 0l;
				count = 0;
				hasError = false;
				tempList = new ArrayList<>();
				meterSum = new Long(0L);
			}
		}
		repository.save(finalList);
		return errorList;
	}

	/**
	 * Process the oldFile format.
	 */
	public Response processOldFile(String fileName) {

		List<MeterReading> meterReadingList = null;
		List<MeterReading> finalMeterReadingList = null;

		try {
			meterReadingList = csvService.processOldMeterReading(fileName);
			finalMeterReadingList = this.saveList(meterReadingList);

			if (finalMeterReadingList.isEmpty()) {
				csvService.delete(fileName, false);
			} else {
				csvService.writeMeterReadingLog(finalMeterReadingList, fileName + "_log");
			}
		} catch (BusinessException e) {
			return new Response(e.getCode(), e.getMessage());
		}
		if(!finalMeterReadingList.isEmpty()){
			return new Response(Constants.CODE_1, Constants.CHECK_LOG_FILE_MESSAGE);
		}
		return new Response(Constants.CODE_0, "");
	}

	/**
	 * Check if there is any reading with consumption problem. (out of range)
	 * @param sumConsumption - Long
	 * @param tempList - List<MeterReading>
	 * @param fractions -  List<Fraction>
	 * @return boolean
	 */
	private boolean verifyConsumptionValidation(Long sumConsumption, List<MeterReading> tempList, List<Fraction> fractions) {

		BigDecimal allowedValue = new BigDecimal(0.25);
		Boolean ok = true;

		Map<String, Fraction> meterReadingMap = convertFractionType(fractions);

		for (MeterReading x : tempList) {

			BigDecimal baseValue = new BigDecimal(sumConsumption).multiply(new BigDecimal(meterReadingMap.get(x.getProfile() + x.getMonth()).getValue()));
			BigDecimal allowedValueBase = baseValue.multiply(allowedValue);

			if (baseValue.add(allowedValueBase).doubleValue() <= x.getConsumption()
					|| baseValue.subtract(allowedValueBase).doubleValue() >= x.getConsumption()) {
				x.setErrorMessage(Constants.CONSUMPTION_NOT_ALLOWED);
				ok = false;
			}
		}
		return ok;
	}

	private Map<String, Fraction> convertFractionType(List<Fraction> fractions) {
		Map<String, Fraction> map = new HashMap<String, Fraction>();
		fractions.forEach((x) -> {
			map.put(x.getProfile() + x.getMonth(), x);
		});
		return map;
	}

}
