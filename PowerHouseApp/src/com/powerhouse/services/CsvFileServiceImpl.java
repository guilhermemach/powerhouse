package com.powerhouse.services;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.powerhouse.constant.MonthShortType;
import com.powerhouse.entity.Fraction;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.exception.BusinessException;
import com.powerhouse.constant.Constants;

/**
 * Class for file csv manipulation.
 * 
 * @author Guilherme
 *
 */
@Component
public class CsvFileServiceImpl implements CsvFileService{

	private final String FRACTION_BASE_PATH = Constants.FILE_SEPARATOR + "files" + Constants.FILE_SEPARATOR + "fraction" + Constants.FILE_SEPARATOR;
	private final String METERREADING_BASE_PATH = Constants.FILE_SEPARATOR + "files" + Constants.FILE_SEPARATOR + "meterreading" + Constants.FILE_SEPARATOR;
	private final static Logger LOGGER = Logger.getLogger(CsvFileServiceImpl.class.getName());

	@Autowired
	private FileService fileService;
	
	/**
	 * Process the fraction oldfile format.
	 */
	public List<Fraction> processOldFraction(String fileName) throws BusinessException {

		CSVParser parser = null;
		FileReader fileReader = null;
		List<Fraction> fractionList = new ArrayList<Fraction>();
		String fullFileName = FRACTION_BASE_PATH + fileName + Constants.CSV;

		try {
			fileReader = fileService.getFileReader(fullFileName);
			parser = new CSVParser(fileReader, Constants.DELIMITER);

			for (CSVRecord record : parser) {
				Fraction f = new Fraction();
				f.setValue(new Double(record.get("Fraction")));
				f.setMonth(MonthShortType.valueOf(record.get("Month")));
				f.setProfile(record.get("Profile"));
				fractionList.add(f);
			}

		} catch (FileNotFoundException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new BusinessException(Constants.CODE_1, "File Not Found = "+ fullFileName);		

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new BusinessException(Constants.CODE_1, "Error in FileReader.");			
		}
		finally {

			try {
				if(fileReader != null && parser!=null){
					fileReader.close();
					parser.close();
				}
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return fractionList;
	}

	/**
	 * Process the meter reading oldfile format.
	 */
	public List<MeterReading> processOldMeterReading(String fileName) throws BusinessException {

		CSVParser parser = null;
		FileReader fileReader = null;
		List<MeterReading> meterReadingList = new ArrayList<MeterReading>();
		String fullFileName = METERREADING_BASE_PATH + fileName + Constants.CSV;

		try {
			fileReader = fileService.getFileReader(fullFileName);
			parser = new CSVParser(fileReader, Constants.DELIMITER);

			for (CSVRecord record : parser) {
				MeterReading m = new MeterReading();
				m.setMeterID(record.get("MeterID"));
				m.setReading(new Long(record.get("Meter Reading")));
				m.setMonth(MonthShortType.valueOf(record.get("Month")));
				m.setProfile(record.get("Profile"));
				meterReadingList.add(m);
			}
		} catch (FileNotFoundException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new BusinessException(Constants.CODE_1,"File Not Found = "+ fullFileName);		

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new BusinessException(Constants.CODE_1,"Error in FileReader.");			
		}
		finally {

			try {
				if(fileReader != null && parser!=null){
					fileReader.close();
					parser.close();
				}
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return meterReadingList;
	}


	/**
	 * Create log for fraction oldfile format.
	 */
	public void writeFractionLog(List<Fraction> fractionList, String fileName) throws BusinessException {

		Object[] FILE_HEADER_MAPPING = { "Month", "Profile", "Fraction", "Error Message" };

		FileWriter fileWriter = null;

		CSVPrinter csvFilePrinter = null;
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(Constants.NEW_LINE_SEPARATOR);

		try {

			fileWriter = fileService.getFileWriter(FRACTION_BASE_PATH + fileName + Constants.CSV);
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
			csvFilePrinter.printRecord(FILE_HEADER_MAPPING);

			for (Fraction f : fractionList) {
				List<String> list = new ArrayList<>();
				list.add(f.getMonth().name());
				list.add(f.getProfile());
				list.add(f.getValue().toString());
				list.add(f.getErrorMessage());
				csvFilePrinter.printRecord(list);
			}

			LOGGER.log(Level.INFO, Constants.FILE_CREATED_SUCCESSFULLY);
		
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new BusinessException(Constants.CODE_1,"Error in CsvFileWriter.");	
			
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();

			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	/**
	 * Create log file for the meter reading oldfile format.
	 */
	public void writeMeterReadingLog(List<MeterReading> fractionList, String fileName) throws BusinessException {

		Object[] FILE_HEADER_MAPPING = { "MeterID", "Profile", "Month", "Meter Reading", "Error Message" };

		FileWriter fileWriter = null;

		CSVPrinter csvFilePrinter = null;
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(Constants.NEW_LINE_SEPARATOR);

		try {

			fileWriter = fileService.getFileWriter(METERREADING_BASE_PATH + fileName + Constants.CSV);
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
			csvFilePrinter.printRecord(FILE_HEADER_MAPPING);

			for (MeterReading m : fractionList) {
				List<String> list = new ArrayList<>();
				list.add(m.getMeterID());
				list.add(m.getProfile());
				list.add(m.getMonth().name());
				list.add(m.getReading().toString());
				list.add(m.getErrorMessage());
				csvFilePrinter.printRecord(list);
			}

			LOGGER.log(Level.INFO, Constants.FILE_CREATED_SUCCESSFULLY);

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new BusinessException(Constants.CODE_1,"Error in CsvFileWriter.");
		}

		finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();

			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	/**
	 * Remove file.
	 */
	public void delete(String fileName, boolean isFractionFile) {
		String fullFileName = null;

		if (isFractionFile) {
			fullFileName = FRACTION_BASE_PATH + fileName +Constants.CSV;
		} else {
			fullFileName = METERREADING_BASE_PATH + fileName + Constants.CSV;
		}
		fileService.remove(fullFileName);
		
	}
}
