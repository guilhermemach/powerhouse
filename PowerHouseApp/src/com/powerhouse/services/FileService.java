package com.powerhouse.services;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public interface FileService {
	
	public FileReader getFileReader(String path) throws IOException;
	
	public void remove(String fullFileName);
	
	public FileWriter getFileWriter(String path) throws IOException;
		

}
