package com.powerhouse.exception;

/**
 * Especialized exception.
 * 
 * @author Guilherme
 *
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msg;
	private Integer code;

	public BusinessException(Integer code, String msg) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}

	public String getMessage() {
		return msg;
	}
	
	public Integer getCode() {
		return code;
	}
}
