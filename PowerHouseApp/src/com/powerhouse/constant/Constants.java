package com.powerhouse.constant;

import java.io.File;
import java.nio.charset.Charset;

import org.apache.commons.csv.CSVFormat;
import org.springframework.http.MediaType;

/**
 * General constants.
 * 
 * @author Guilherme
 *
 */
public abstract class Constants {
	
	public static String DATA_INTEGRITY_PROBLEM = "Meter reading is incorrect.";
	public static String DATA_WITH_NO_FRACTION = "Meter reading with no fraction.";
	public static String CONSUMPTION_NOT_ALLOWED = "Consumption not allowed.";
	public static String INCONSISTENT_DATA = "Data is incosistent.";
	public static String CHECK_LOG_FILE_MESSAGE = "Please check the log file for more details.";
	public static String FILE_CREATED_SUCCESSFULLY = "CSV file was created successfully.";
	
	public static final String FILE_SEPARATOR = File.separator;
	public static final String NEW_LINE_SEPARATOR = "\n";
	public static final CSVFormat DELIMITER = CSVFormat.RFC4180.withHeader().withDelimiter(',');
	public static final String CSV = ".csv";	
	
	public static final MediaType JSON_MEDIA_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	
	
	public static Integer CODE_1 =1;
	public static Integer CODE_0 =0;

}
