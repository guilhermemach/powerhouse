package com.powerhouse.constant;

/**
 * Type for months.
 * 
 * @author Guilherme
 *
 */
public enum MonthShortType {
	
	JAN, FEB, MAR, APR, MAI, JUN, JUL, AUG, SEP, OCT, NOV, DEC;

}
