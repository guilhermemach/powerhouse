package com.powerhouse.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.powerhouse.entity.Fraction;

/**
 * Repository for fraction / profile
 * 
 * @author Guilherme
 *
 */
public interface FractionRepository extends CrudRepository<Fraction, Integer> {	
	
	public List<Fraction> findByProfile(String profile);

}

