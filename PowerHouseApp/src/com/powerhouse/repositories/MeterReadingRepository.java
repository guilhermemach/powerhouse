package com.powerhouse.repositories;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.powerhouse.constant.MonthShortType;
import com.powerhouse.entity.MeterReading;

/**
 * Repository for meter reading.
 * 
 * @author Guilherme
 *
 */
public interface MeterReadingRepository extends CrudRepository<MeterReading, Integer> {	
	
	public List<MeterReading> getMeterReadingByMeterId(String meterId);
	
	public MeterReading getConsumptionByMetterIdAndMonth(String meterId, MonthShortType month);
 
}
