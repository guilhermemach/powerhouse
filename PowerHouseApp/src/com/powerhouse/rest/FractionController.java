package com.powerhouse.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.powerhouse.entity.Fraction;
import com.powerhouse.services.FractionService;

/**
 * Rest service controller for fraction/ profile.
 * 
 * @author Guilherme
 *
 */
@RestController
public class FractionController {

	@Autowired
	private FractionService service;

	@RequestMapping(value = "/fraction", method = RequestMethod.GET)
	public @ResponseBody List<Fraction> getAll() {
		return service.getAll();
	}

	@RequestMapping(value = "/fraction/{id}", method = RequestMethod.GET)
	public @ResponseBody Fraction getByID(@PathVariable("id") Integer id) {
		return service.getByID(id);
	}

	@RequestMapping(value = "/fraction", method = RequestMethod.PUT)
	public @ResponseBody Fraction update(@RequestBody Fraction obj) {
		return saveOrUpdate(obj);
	}

	@RequestMapping(value = "/fraction", method = RequestMethod.POST)
	public @ResponseBody List<Fraction> saveList(@RequestBody List<Fraction> list) {
		return service.saveList(list);
	}

	@RequestMapping(value = "/fraction/{id}", method = RequestMethod.DELETE)
	public void removeByID(@PathVariable("id") Integer id) {
		service.removeByID(id);
	}

	@RequestMapping(value = "/fraction/oldfile/{file}", method = RequestMethod.GET)
	public @ResponseBody Response processOldFractionFile(@PathVariable("file")String fileName) {
		return service.processOldFile(fileName);
	}
	
	private Fraction saveOrUpdate(Fraction obj) {
		return service.saveOrUpdate(obj);
	}
}