package com.powerhouse.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.powerhouse.constant.MonthShortType;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.services.MeterReadingService;

/**
 * Meter Reading rest controller.
 * 
 * @author Guilherme
 *
 */
@RestController
public class MeterReadingController {
	
	@Autowired
	private MeterReadingService service;

	@RequestMapping(value = "/meterreading", method = RequestMethod.GET)
	public @ResponseBody List<MeterReading> getAll() {
		return  service.getAll();
	}
	
	@RequestMapping(value = "/meterreading/{id}", method = RequestMethod.GET)
	public @ResponseBody MeterReading getByID(@PathVariable("id") Integer id) {
		return service.getByID(id);
	}
	
	@RequestMapping(value = "/consumption/{meterID}", method = RequestMethod.GET)
	public @ResponseBody List<MeterReading>  getConsumptionByMetterID(@PathVariable("meterID") String meterID) {
		return service.getMeterReadingByMeterID(meterID);
	}
	
	@RequestMapping(value = "/consumption/{meterID}/{mounth}", method = RequestMethod.GET)
	public @ResponseBody MeterReading  getConsumptionByMeterIdAndMonth(@PathVariable("meterID") String meterID, @PathVariable("mounth") MonthShortType month) {
		return service.getConsumptionByMetterIdAndMonth(meterID, month);
	}	
	
	@RequestMapping(value = "/meterreading", method = RequestMethod.PUT)
	public @ResponseBody MeterReading update(@RequestBody MeterReading obj) {		
		return saveOrUpdate(obj);
	}
	
	@RequestMapping(value = "/meterreading", method = RequestMethod.POST)
	public @ResponseBody List<MeterReading> saveList(@RequestBody List<MeterReading> list) {		
		return service.saveList(list);
	}
	
	@RequestMapping(value = "/meterreading/{id}", method = RequestMethod.DELETE)
	public void removeByID(@PathVariable("id") Integer id) {
		service.removeByID(id);		
	}

	@RequestMapping(value = "/meterreading/oldfile/{file}", method = RequestMethod.GET)
	public @ResponseBody Response processOldFractionFile(@PathVariable("file")String fileName) {
		return service.processOldFile(fileName);
	}
	
	private MeterReading saveOrUpdate(MeterReading obj){
		return service.saveOrUpdate(obj);
	}	
}