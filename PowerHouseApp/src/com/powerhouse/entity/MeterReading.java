package com.powerhouse.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import com.powerhouse.constant.MonthShortType;

/**
 * Meter Reading - entity representation.
 * 
 * @author Guilherme
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "MeterReading.getMeterReadingByMeterId", query = "SELECT m FROM MeterReading m WHERE m.meterID = ?1"),
	@NamedQuery(name = "MeterReading.getConsumptionByMetterIdAndMonth", query = "SELECT m FROM MeterReading m WHERE m.meterID = ?1 and m.month = ?2")
})
public class MeterReading {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String meterID;
	private String profile;
	private MonthShortType month;
	private Long reading;
	private Long consumption;
	@Transient
	private String errorMessage;
	
	
	public MeterReading(String meterID, String profile, MonthShortType month, Long reading, Long consumption) {
		this.meterID = meterID;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
		this.consumption = consumption;
	}
	
	public MeterReading(String meterID, String profile, MonthShortType month, Long reading) {
		this.meterID = meterID;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
	}
	
	public MeterReading(){};
	
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public MonthShortType getMonth() {
		return month;
	}
	public void setMonth(MonthShortType month) {
		this.month = month;
	}
	public Long getReading() {
		return reading;
	}
	public void setReading(Long reading) {
		this.reading = reading;
	}
	public String getMeterID() {
		return meterID;
	}
	public void setMeterID(String meterID) {
		this.meterID = meterID;
	}
	public Long getConsumption() {
		return consumption;
	}
	public void setConsumption(Long consumption) {
		this.consumption = consumption;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((meterID == null) ? 0 : meterID.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeterReading other = (MeterReading) obj;
		if (meterID == null) {
			if (other.meterID != null)
				return false;
		} else if (!meterID.equals(other.meterID))
			return false;	
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (month != other.month)
			return false;
		
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}	
	
	

}
