package com.powerhouse.entity;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import com.powerhouse.constant.MonthShortType;

/**
 * Fraction / Profile - entity representation.
 * 
 * @author Guilherme
 *
 */
@NamedQuery(name = "Fraction.findByProfile", query = "SELECT f FROM Fraction f WHERE f.profile = ?1")
@Entity
public class Fraction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String profile;
	private Double value;
	private MonthShortType month;
	@Transient
	private String errorMessage;
	
		
	public Fraction(String profile, Double value, MonthShortType month) {
		this.profile = profile;
		this.value = value;
		this.month = month;
	}

	public Fraction(){};
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public Double getValue() {
		return value ;
	}
	public void setValue(Double value) {
		this.value = value;
	}

	public MonthShortType getMonth() {
		return month;
	}

	public void setMonth(MonthShortType month) {
		this.month = month;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fraction other = (Fraction) obj;
		if ( value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (month != other.month)
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		return true;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
