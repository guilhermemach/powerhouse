package com.powerhouse.tests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerhouse.Main;
import com.powerhouse.constant.MonthShortType;
import com.powerhouse.constant.Constants;
import com.powerhouse.entity.Fraction;
import com.powerhouse.repositories.FractionRepository;

/** 
 * Class test for Fraction Rest integration call.
 * 
 * @author Guilherme
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class FractionIntegrationTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private FractionRepository repository;

	private List<Fraction> list;

	@Before
	public void setup() throws Exception {		
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		list = new ArrayList<>();
		list.addAll((Collection<? extends Fraction>) repository.save(new HelperTest().getMockFractionList()));
	}

	@Test
	public void getAllTest() throws Exception {
		mockMvc.perform(get("/fraction").contentType(Constants.JSON_MEDIA_TYPE)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(36))).andExpect(status().isOk());
	}

	@Test
	public void findByIdTest() throws Exception {
		mockMvc.perform(get("/fraction/" + list.get(0).getId()).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.profile", is("A"))).andExpect(jsonPath("$.month", is(MonthShortType.JAN.name())))
				.andExpect(status().isOk());
	}

	@Test
	public void fractionNotFound() throws Exception {
		mockMvc.perform(get("/fraction/1/").contentType(Constants.JSON_MEDIA_TYPE)).andExpect(content().string(""))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteByIdTest() throws Exception {
		mockMvc.perform(delete("/fraction/" + list.get(0).getId()).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(status().isOk());
	}

	/**
	 * save a list of more than one 12 items block.
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveListTest() throws Exception {
		
		repository.deleteAll();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(new HelperTest().getMockFractionList());

		mockMvc.perform(post("/fraction/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(0))).andExpect(status().isOk());
		
		List<Fraction> response = (List<Fraction>)repository.findAll();
		assertEquals(36, response.size());
	}

	/**
	 * save one 12 items block test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveOneBlockTest() throws Exception {

		repository.deleteAll();
		List<Fraction> list = new ArrayList<>();
		list.addAll(new HelperTest().getMockFractionList().subList(0, 12));

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/fraction/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(0))).andExpect(status().isOk());
		
		List<Fraction> response = (List<Fraction>)repository.findAll();
		assertEquals(12, response.size());
	}

	@Test
	public void updateTest() throws Exception {

		list.get(0).setMonth(MonthShortType.AUG);
		list.get(0).setProfile("C");
		list.get(0).setValue(0.55);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(list.get(0));

		mockMvc.perform(put("/fraction/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.month", is(MonthShortType.AUG.name())))
				.andExpect(jsonPath("$.profile", is(list.get(0).getProfile())))
				.andExpect(jsonPath("$.value", is(list.get(0).getValue()))).andExpect(status().isOk());
	}

	/**
	 * save a correct unordered list test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveUnorderedListTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<Fraction> list = new HelperTest().getMockFractionList();
		Collections.shuffle(list);
		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/fraction/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(0))).andExpect(status().isOk());
	}

	/**
	 * Partial error while saving a list test.
	 *
	 * @throws Exception
	 */
	@Test
	public void partialErrorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<Fraction> list = new HelperTest().getMockFractionList();
		list.get(0).setValue(new Double(1.));
		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/fraction/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(12)))
				.andExpect(jsonPath("$[0].errorMessage", is(Constants.INCONSISTENT_DATA))).andExpect(status().isOk());
	}

	/**
	 * All list not integrity test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void errorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<Fraction> list = new HelperTest().getMockFractionList();
		list.get(0).setValue(new Double(1.));
		list.get(12).setValue(new Double(1.));
		list.get(24).setValue(new Double(1.));
		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/fraction/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(36)))
				.andExpect(jsonPath("$[0].errorMessage", is(Constants.INCONSISTENT_DATA)))
				.andExpect(jsonPath("$[12].errorMessage", is(Constants.INCONSISTENT_DATA)))
				.andExpect(jsonPath("$[24].errorMessage", is(Constants.INCONSISTENT_DATA))).andExpect(status().isOk());
	}

	@After
	public void clearup() throws Exception {
		repository.deleteAll();
	}

}
