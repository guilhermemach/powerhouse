package com.powerhouse.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerhouse.Main;
import com.powerhouse.constant.Constants;
import com.powerhouse.constant.MonthShortType;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.repositories.FractionRepository;
import com.powerhouse.repositories.MeterReadingRepository;
import com.powerhouse.services.MeterReadingService;

/** 
 * Class test for Meter Reading Rest integration call.
 * 
 * @author Guilherme
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class MeterReadingIntegrationTest {

	private MockMvc mockMvc;
	private List<MeterReading> list;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private FractionRepository fractionRepository;
	
	@Autowired
	private MeterReadingService service;

	@Autowired
	private MeterReadingRepository meterReadingRepositorty;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		fractionRepository.save(new HelperTest().getMockFractionList());
		list = (List<MeterReading>) meterReadingRepositorty.save(new HelperTest().getMockMeterReadingList());
	}

	@Test
	public void getAllTest() throws Exception {
		mockMvc.perform(get("/meterreading").contentType(Constants.JSON_MEDIA_TYPE)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(36)));
	}

	@Test
	public void findByIdTest() throws Exception {
		mockMvc.perform(get("/meterreading/" + list.get(0).getId()).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.profile", is("A"))).andExpect(jsonPath("$.month", is(MonthShortType.JAN.name())))
				.andExpect(jsonPath("$.reading", is(10))).andExpect(status().isOk());
	}

	/**
	 * Save one simple block list json test (12 items).
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveOneBlockTest() throws Exception {

		meterReadingRepositorty.deleteAll();
		List<MeterReading> list = new ArrayList<>();		
		list.addAll(new HelperTest().getMockMeterReadingList().subList(0, 12));
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(0))).andExpect(status().isOk());		
		
		List<MeterReading> response = (List<MeterReading>)meterReadingRepositorty.findAll();
		assertEquals(12, response.size());
	}
	

	/**
	 * save a list of more than one 12 items block.
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveListTest() throws Exception {

		meterReadingRepositorty.deleteAll();
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(new HelperTest().getMockMeterReadingList());

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(0))).andExpect(status().isOk());
		
		List<MeterReading> response = (List<MeterReading>)meterReadingRepositorty.findAll();
		assertEquals(36, response.size());
	}
	
	/**
	 * Save unordered json list test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveUnorderedListTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<MeterReading> list= new HelperTest().getMockMeterReadingList();
		Collections.shuffle(list);
	
		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(0))).andExpect(status().isOk());
	}

	/**
	 * Data reading error validations test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void errorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<MeterReading> list = new HelperTest().getMockMeterReadingList();
		long var = 10l;
		list.get(11).setReading(var);
		list.get(23).setReading(var);
		list.get(35).setReading(var);

		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(36)))
				.andExpect(jsonPath("$[11].errorMessage", is(Constants.DATA_INTEGRITY_PROBLEM)))
				.andExpect(jsonPath("$[23].errorMessage", is(Constants.DATA_INTEGRITY_PROBLEM)))
				.andExpect(jsonPath("$[35].errorMessage", is(Constants.DATA_INTEGRITY_PROBLEM)))
				.andExpect(status().isOk());
	}

	/**
	 * Partial reading error validations test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void partialErrorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<MeterReading> list = new HelperTest().getMockMeterReadingList();
		long var = 10l;
		list.get(11).setReading(var);
		list.get(5).setReading(var);

		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(12)))
				.andExpect(jsonPath("$[5].errorMessage", is(Constants.DATA_INTEGRITY_PROBLEM)))
				.andExpect(jsonPath("$[11].errorMessage", is(Constants.DATA_INTEGRITY_PROBLEM)))
				.andExpect(status().isOk());
	}
	
	/**
	 * Consumption range (min and max) not allowed test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void consumptionNotAllowedErrorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<MeterReading> list = new HelperTest().getMockMeterReadingList();
		list.get(11).setReading(115l);
		list.get(5).setReading(55l);

		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(12)))
				.andExpect(jsonPath("$[5].errorMessage", is(Constants.CONSUMPTION_NOT_ALLOWED)))
				.andExpect(jsonPath("$[6].errorMessage", is(Constants.CONSUMPTION_NOT_ALLOWED)))
				.andExpect(jsonPath("$[11].errorMessage", is(Constants.CONSUMPTION_NOT_ALLOWED)))
				.andExpect(status().isOk());
	}
	
	/**
	 * Meter reading without a fraction/profile test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void noFractionErrorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<MeterReading> list = new HelperTest().getMockMeterReadingList();
		list.forEach((var) -> {
			if(var.getProfile().equals("A")){
				var.setProfile("N");
			}
		});

		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(12)))
				.andExpect(jsonPath("$[0].errorMessage", is(Constants.DATA_WITH_NO_FRACTION)))
				.andExpect(status().isOk());
	}
	
	/**
	 * Combination test (Meter reading without a fraction/profile and meter reading error validations test)
	 * 
	 * @throws Exception
	 */
	@Test
	public void twoValidationErrorSaveTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<MeterReading> list = new HelperTest().getMockMeterReadingList();
		
		list.get(1).setReading(5l);
		
		list.forEach((var) -> {
			if(var.getProfile().equals("B")){
				var.setProfile("N");
			}
		});

		String json = mapper.writeValueAsString(list);

		mockMvc.perform(post("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$", hasSize(24)))
				.andExpect(jsonPath("$[1].errorMessage", is(Constants.DATA_INTEGRITY_PROBLEM)))
				.andExpect(jsonPath("$[12].errorMessage", is(Constants.DATA_WITH_NO_FRACTION)))
				.andExpect(status().isOk());
	}

	@Test
	public void updateTest() throws Exception {

		list.get(0).setMonth(MonthShortType.AUG);
		list.get(0).setProfile("C");
		list.get(0).setReading(11l);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(list.get(0));

		mockMvc.perform(put("/meterreading/").content(json).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.month", is(MonthShortType.AUG.name())))
				.andExpect(jsonPath("$.profile", is(list.get(0).getProfile())))
				.andExpect(jsonPath("$.reading", is(Integer.parseInt(list.get(0).getReading().toString()))))
				.andExpect(status().isOk());
	}	
	
	@Test
	public void readMeterNotFound() throws Exception {
		meterReadingRepositorty.deleteAll();		
		mockMvc.perform(get("/meterreading/1/").contentType(Constants.JSON_MEDIA_TYPE))		
		.andExpect(content().string(""))
		.andExpect(status().isOk());
	}
	
	/**
	 * Consumption test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findConsumptionTest() throws Exception {

		meterReadingRepositorty.deleteAll();
		service.saveList(new HelperTest().getMockMeterReadingList());
		
		mockMvc.perform(get("/consumption/0001/" + MonthShortType.JAN.name()).contentType(Constants.JSON_MEDIA_TYPE))
		.andExpect(jsonPath("$.profile", is("A")))
		.andExpect(jsonPath("$.consumption", is(10)))
		.andExpect(status().isOk());
	}
	
	@Test
	public void consumptionNotFoundTest() throws Exception {
		meterReadingRepositorty.deleteAll();		
		mockMvc.perform(get("/consumption/0001/" + MonthShortType.JAN.name()).contentType(Constants.JSON_MEDIA_TYPE))		
		.andExpect(content().string(""))
		.andExpect(status().isOk());
	}

	@Test
	public void deleteByIdTest() throws Exception {
		mockMvc.perform(delete("/meterreading/" + list.get(0).getId()).contentType(Constants.JSON_MEDIA_TYPE))
				.andExpect(status().isOk());
	}

	@After
	public void clearup() throws Exception {
		meterReadingRepositorty.deleteAll();
		fractionRepository.deleteAll();
	}

}
