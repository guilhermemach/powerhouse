package com.powerhouse.tests;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVParser;
import org.junit.Ignore;

import com.powerhouse.constant.Constants;
import com.powerhouse.constant.MonthShortType;
import com.powerhouse.entity.Fraction;
import com.powerhouse.entity.MeterReading;

/**
 * Class to help on junit tests.
 * 
 * @author Guilherme
 *
 */
@Ignore
public class HelperTest {

	protected List<Fraction> getMockFractionList() {

		List<Fraction> finalList = new ArrayList<Fraction>();

		List<Fraction> list1 = new ArrayList<Fraction>();
		for (int i = 0; i < 12; i++) {
			list1.add(new Fraction("A", new Double(0.08), MonthShortType.values()[i]));
		}
		list1.get(0).setValue(new Double(0.10));
		list1.get(1).setValue(new Double(0.10));

		List<Fraction> list2 = new ArrayList<Fraction>();
		for (int i = 0; i < 12; i++) {
			list2.add(new Fraction("B", new Double(0.08), MonthShortType.values()[i]));
		}
		list2.get(0).setValue(new Double(0.10));
		list2.get(1).setValue(new Double(0.10));

		List<Fraction> list3 = new ArrayList<Fraction>();
		for (int i = 0; i < 12; i++) {
			list3.add(new Fraction("C", new Double(0.08), MonthShortType.values()[i]));
		}
		list3.get(0).setValue(new Double(0.10));
		list3.get(1).setValue(new Double(0.10));

		finalList.addAll(list1);
		finalList.addAll(list2);
		finalList.addAll(list3);

		return finalList;
	}

	protected List<MeterReading> getMockMeterReadingList() {

		List<MeterReading> finalList = new ArrayList<MeterReading>();
		List<MeterReading> list1 = new ArrayList<MeterReading>();
		Long reading = 10l;
		for (int i = 0; i < 12; i++) {
			list1.add(new MeterReading("0001", "A", MonthShortType.values()[i], reading));
			reading += 10l;
		}

		List<MeterReading> list2 = new ArrayList<MeterReading>();
		Long reading2 = 8l;
		for (int i = 0; i < 12; i++) {
			list2.add(new MeterReading("0002", "B", MonthShortType.values()[i], reading2));
			reading2 += 8l;
		}

		List<MeterReading> list3 = new ArrayList<MeterReading>();
		Long reading3 = 20l;
		for (int i = 0; i < 12; i++) {
			list3.add(new MeterReading("0003", "C", MonthShortType.values()[i], reading3));
			reading3 += 20l;
		}

		finalList.addAll(list1);
		finalList.addAll(list2);
		finalList.addAll(list3);

		return finalList;
	}

	/**
	 * Return size of a file log.
	 * 
	 * @param fileName
	 * @return Integer
	 * @throws IOException
	 */
	public Integer getSizeOldFile(String fileName) throws IOException {

		CSVParser parser = null;
		FileReader fileReader = null;
		Integer size =null;
		
		try {
			fileReader = new FileReader(fileName);
			parser = new CSVParser(fileReader, Constants.DELIMITER);
			size = parser.getRecords().size();
		} finally {

			try {
				if (fileReader != null && parser != null) {
					fileReader.close();
					parser.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return size;
	}

}
