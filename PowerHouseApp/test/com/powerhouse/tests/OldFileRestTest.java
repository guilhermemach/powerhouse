package com.powerhouse.tests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.powerhouse.Main;
import com.powerhouse.constant.Constants;
import com.powerhouse.rest.FractionController;
import com.powerhouse.rest.MeterReadingController;
import com.powerhouse.rest.Response;
import com.powerhouse.services.FractionService;
import com.powerhouse.services.MeterReadingService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class OldFileRestTest{


	private MockMvc mockMvc;	
	
	@InjectMocks	
	private MeterReadingController mrController;
	
	@InjectMocks	
	private FractionController controller;
	
	@Mock
	private FractionService fractioService;
	
	@Mock
	private MeterReadingService mrService;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
	    this.mockMvc = MockMvcBuilders.standaloneSetup(mrController,controller).build();
	}

	@Test
	public void processFractionOldFileTest() throws Exception {
		
		Mockito.when(fractioService.processOldFile("test")).thenReturn(new Response(0, ""));
		mockMvc.perform(get("/fraction/oldfile/test").contentType(Constants.JSON_MEDIA_TYPE))
		.andExpect(jsonPath("$.code", is(0))).andExpect(status().isOk());
	}
	
	@Test
	public void errorProcessFractionOldFileTest() throws Exception {
		
		Response response = new Response(1, "file not found");
		Mockito.when(fractioService.processOldFile("test")).thenReturn(response);
		mockMvc.perform(get("/fraction/oldfile/test").contentType(Constants.JSON_MEDIA_TYPE))
		.andExpect(jsonPath("$.code", is(response.getCode())))
		.andExpect(jsonPath("$.message", is(response.getMessage())))
		.andExpect(status().isOk());
	}
	
	@Test
	public void processMeterReadingOldFileTest() throws Exception {
		
		Mockito.when(mrService.processOldFile("test")).thenReturn(new Response(0, ""));
		mockMvc.perform(get("/meterreading/oldfile/test").contentType(Constants.JSON_MEDIA_TYPE))
		.andExpect(jsonPath("$.code", is(0))).andExpect(status().isOk());
	}
	
	@Test
	public void errorProcessMeterReadingOldFileTest() throws Exception {
		
		Response response = new Response(1, "file not found");
		Mockito.when(mrService.processOldFile("test")).thenReturn(response);
		mockMvc.perform(get("/meterreading/oldfile/test").contentType(Constants.JSON_MEDIA_TYPE))
		.andExpect(jsonPath("$.code", is(response.getCode())))
		.andExpect(jsonPath("$.message", is(response.getMessage())))
		.andExpect(status().isOk());
	}	
}
