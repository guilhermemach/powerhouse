package com.powerhouse.tests;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;


import com.powerhouse.Main;
import com.powerhouse.constant.Constants;
import com.powerhouse.entity.Fraction;
import com.powerhouse.entity.MeterReading;
import com.powerhouse.services.CsvFileService;
import com.powerhouse.services.FileService;

@SpringApplicationConfiguration(classes = Main.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class FileServiceTest{
	
	@Autowired
	@InjectMocks
	private CsvFileService csvService;
	
	private final String FRACTION_BASE_PATH = Constants.FILE_SEPARATOR + "files" + Constants.FILE_SEPARATOR + "fraction" + Constants.FILE_SEPARATOR;
	private final String METERREADING_BASE_PATH = Constants.FILE_SEPARATOR + "files" + Constants.FILE_SEPARATOR + "meterreading" + Constants.FILE_SEPARATOR;
	
	@Mock
	private FileService fileService;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void readFractionOldFileTest() throws Exception {
		  String file = getClass().getResource("fraction_test.csv").getPath();
		  String pathMock = FRACTION_BASE_PATH + "fraction_test.csv";
		  Mockito.when(fileService.getFileReader(pathMock)).thenReturn(new FileReader(file));
		  List<Fraction> list =  csvService.processOldFraction("fraction_test");		  
		  assertEquals(24, list.size());
	}
	
	@Test
	public void readMeterReadingOldFileTest() throws Exception {
		  String file = getClass().getResource("meterreading_test.csv").getPath();
		  String pathMock = METERREADING_BASE_PATH + "meterreading_test.csv";
		  Mockito.when(fileService.getFileReader(pathMock)).thenReturn(new FileReader(file));
		  List<MeterReading> list =  csvService.processOldMeterReading("meterreading_test");		  
		  assertEquals(24, list.size());
	}
		
	@Test
	public void writeMeterReadingOldFileTest() throws Exception {
		  String file = getClass().getResource("meterreading_test.csv").getPath();
		  String pathMock = METERREADING_BASE_PATH + "meterreading_test_log.csv";
		  file = file.replace("test.csv", "test_log.csv");
		  Mockito.when(fileService.getFileWriter(pathMock)).thenReturn(new FileWriter(file));
		  csvService.writeMeterReadingLog( new HelperTest().getMockMeterReadingList(),"meterreading_test_log");
		  assertSame(36, new HelperTest().getSizeOldFile(file));
	}	
	
	@Test
	public void writeFractonOldFileTest() throws Exception {
		  String file = getClass().getResource("fraction_test.csv").getPath();
		  String pathMock = FRACTION_BASE_PATH + "fraction_test_log.csv";
		  file = file.replace("test.csv", "test_log.csv");
		  Mockito.when(fileService.getFileWriter(pathMock)).thenReturn(new FileWriter(file));
		  csvService.writeFractionLog( new HelperTest().getMockFractionList(),"fraction_test_log");
		  assertSame(36, new HelperTest().getSizeOldFile(file));
	}
}
